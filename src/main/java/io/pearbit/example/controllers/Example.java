package io.pearbit.example.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.pearbit.example.dao.sql.UserDAO;
import io.pearbit.example.models.Rol;
import io.pearbit.example.models.User;

/**
 * Servlet implementation class Example
 */
public class Example extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDAO dao;
	private ObjectMapper mapper;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Example() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json;charset=UTF-8");
		PrintWriter out = response.getWriter();
		try {
			dao = new UserDAO(); // Objeto DAO de persistencia de usuario
			mapper = new ObjectMapper(); // Mapper para general Json

			String username = request.getParameter("username"); // Parametros enviados en la solicitud
			String password = request.getParameter("password");

			User user = null;
			String jsonUser = "";
			try {
				user = dao.consultaIndividual(username); // se realiza la consulta a través del username.
				jsonUser = mapper.writeValueAsString(user); // creamos el Json
				
				if (user != null) { //si el usuario no esta vacio 
					if (user.getPassword().equals(password)) { // comparamos si la contraseña corresponde con la
																// guardada en la base de datos
						jsonUser = mapper.writeValueAsString(user);
					} else { //Si algo sale mal creamos JSON de respuesta de error 
						Map<String, Object> errorMap = new HashMap<>();
						errorMap.put("ERROR", "001");
						errorMap.put("CAUSE", "El usuario es correcto, sin embargo la contraseña es incorrecta");
						errorMap.put("USERNAME", username);
						jsonUser = mapper.writeValueAsString(errorMap);
					}
				}

			} catch (SQLException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}

			System.out.println(jsonUser);

			out.print(jsonUser); //enviamos al cliente el JSON 

		} finally {
			out.close();
		}

	}

}
