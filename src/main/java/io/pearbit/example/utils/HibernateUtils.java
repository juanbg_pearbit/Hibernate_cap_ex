package io.pearbit.example.utils;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


/**
 * Class to be tool for hibernate
 * @author JuanBG
 *
 */
public class HibernateUtils {

	//Session factory will allow connections to db 
	private static final SessionFactory sessionFactory = buildSessionFactory();

	//
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	
	/**
	 * 
	 * @return
	 */
	private static SessionFactory buildSessionFactory() {
		try {
			// Create the SessionFactory from hibernate.cfg.xml
			return new Configuration().configure().buildSessionFactory();
		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static void shutdown() {
		// Close caches and connection pools
		getSessionFactory().close();
	}
}
